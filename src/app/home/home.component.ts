import { Component, OnInit } from '@angular/core';
import { HeroService } from "../repository/hero.service";
import { Hero } from '../entity/hero';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  //les propriétés d'un component sont les seuls variables qui seront
  //accessible dans le template du component
  heroes:Hero[];
  /**
   * On crée une propriété newHero qui contiendra un Hero qu'on pourra 
   * lier à un formulaire côté template
   */
  newHero:Hero = {
    name:null,
    birthdate: null,
    title: null,
    level: null
  };
  /**
   * Propriété qui contiendra le hero actuellement sélectionné
   */
  selected:Hero;
  
  //Ici, on injecte notre service dans notre component, on pourra s'en
  //servir directement dans les méthodes du component
  constructor(private repo:HeroService) { }

  ngOnInit() {    
    /**
     * On utilise notre HeroService pour déclencher sa méthode findAll(),
     * celle ci renvoie un Observable sur lequel on va se subscribe 
     * (le subscribe est l'équivalent du then() pour les promises, c'est
     * lui qui va déclencher la requête http pour de vrai).
     * A l'intérieur du subscribe, on met une fonction anonyme, ici sous
     * forme de fat arrow function.
     * Le paramètre de la fonction du subscribe sera le contenu de la 
     * réponse de la requête http faite vers le serveur. On la met dans
     * la variable data puis on assigne la valeur de data à notre propriété
     * this.heroes afin de rendre les data accessibles au template
     */
    this.repo.findAll().subscribe(data => this.heroes = data);
  }
  /**
   * On crée une méthode dans le component afin de pouvoir y accéder
   * depuis le template. Celle ci va déclencher la méthode add du 
   * HeroService
   */
  addHero() {
    this.repo.add(this.newHero).subscribe(hero => this.heroes.push(hero));
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { OneHeroComponent } from './one-hero/one-hero.component';
import { ExampleInputComponent } from './example-input/example-input.component';
import { RegisterComponent } from './register/register.component';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';
import { JwtService } from './interceptor/jwt.service';
import { LandingComponent } from './landing/landing.component';
import { AccountComponent } from './account/account.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    OneHeroComponent,
    ExampleInputComponent,
    RegisterComponent,
    HeaderComponent,
    LoginComponent,
    LandingComponent,
    AccountComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: JwtService, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

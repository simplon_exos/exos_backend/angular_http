import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-example-input',
  templateUrl: './example-input.component.html',
  styleUrls: ['./example-input.component.scss']
})
export class ExampleInputComponent implements OnInit {

  @Input()
  name:string;
  
  constructor() { }

  ngOnInit() {
  }

}

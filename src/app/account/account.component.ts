import { Component, OnInit } from '@angular/core';
import { AuthService } from '../repository/auth.service';
import { User } from '../entity/user';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  user:User;
  constructor(private authService:AuthService) { }

  ngOnInit() {
    this.authService.user.subscribe(user => this.user = user);
  }

}
